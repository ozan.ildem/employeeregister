'use strict';

describe('EmployeeController', function() {
    var $httpBackend, $rootScope, createController;

    // Set up the module
    beforeEach(module('myApp.EmployeeRegister'));

    beforeEach(inject(function($injector) {
        // Set up the mock http service responses
        $httpBackend = $injector.get('$httpBackend');
        // backend definition common for all tests
        $httpBackend.when('GET', 'http://localhost:4200/')
            .respond(200, [{firstname: 'Max', lastname: 'Mustermann', phonenumber: '123456', email: 'Max@example.com'},
                {firstname: 'Hans', lastname: 'Mustermuster', phonenumber: '654321', email: 'Hans@example.com'},
                {firstname: 'Peter', lastname: 'mannmann', phonenumber: '246801', email: 'Peter@example.com'}]);
        $httpBackend.when('POST', 'http://localhost:4200/add').respond(200);
        $httpBackend.when('DELETE', 'http://localhost:4200/delete/1').respond(200);
        $httpBackend.when('GET', 'http://localhost:4200/1').respond(200, {firstname: 'Max', lastname: 'Mustermann', phonenumber: '123456', email: 'Max@example.com'})
        $httpBackend.when('GET', 'http://localhost:4200/handshake').respond(200,"Hello World")
        // Get hold of a scope (i.e. the root scope)
        $rootScope = $injector.get('$rootScope');
        $rootScope.employees = {};
        // The $controller service is used to create instances of controllers
        var $controller = $injector.get('$controller');

        createController = function() {
            return $controller('EmployeeRegisterController', {'$scope' : $rootScope });
        };
        createController();
        $rootScope.id = 1;
        $rootScope.getEmployees();
        $rootScope.postEmployee("","","","");
        $rootScope.deleteEmployeesWithID();
        $rootScope.getEmployeesByID();
        $rootScope.getRequest();
    }));


    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    it('should do get request', function(){

        $httpBackend.expect('GET','http://localhost:4200/').respond(200, [{firstname: 'Max', lastname: 'Mustermann', phonenumber: '123456', email: 'Max@example.com'},
            {firstname: 'Hans', lastname: 'Mustermuster', phonenumber: '654321', email: 'Hans@example.com'},
            {firstname: 'Peter', lastname: 'mannmann', phonenumber: '246801', email: 'Peter@example.com'}]);
        $httpBackend.flush();
    });

    it('should do post requests', function () {
        $httpBackend.expect('POST', 'http://localhost:4200/add').respond(200);
        $httpBackend.flush();
    })

    it('should do delete requests', function () {
        $httpBackend.expect('DELETE', 'http://localhost:4200/delete/1').respond(200);
        $httpBackend.flush();
    })

    it('should find by ID', function (){
        $httpBackend.expect('GET', 'http://localhost:4200/1').respond(200, {firstname: 'Max', lastname: 'Mustermann', phonenumber: '123456', email: 'Max@example.com'})
        $httpBackend.flush();
    })

    it('should be able to do the handshake', function (){
        $httpBackend.expect('GET', 'http://localhost:4200/handshake')
        $httpBackend.flush();
    })

});
