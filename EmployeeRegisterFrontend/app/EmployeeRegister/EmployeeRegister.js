'use strict';
/*jshint esversion: 6 */

var EmployeeRegister = angular.module("myApp.EmployeeRegister", [])

EmployeeRegister.controller("EmployeeRegisterController", function($scope, $http) {
    $scope.home = "This is the homepage";

    /*
    * The Handshake test function
    **/
    $scope.getRequest = function() {
        console.log("I've been pressed!");
        $http.get("http://localhost:4200/handshake").then(
            function successCallback(response) {
                $scope.response = response;
                console.log(response);
            },
            function errorCallback(response) {
            }
        );
    };


    /*
    * The function to retrieve all employees from the API
    **/
    $scope.getEmployees = function (){
        $scope.employees = [];
        $http.get("http://localhost:4200/").then(
            function successCallback(employees) {
                $scope.employees = employees;
                console.log(employees);
            },
            function errorCallback(employees) {
            }
        );
    };

    $scope.firstname = null;
    $scope.lastname = null;
    $scope.phonenumber = null;
    $scope.mail = null;

    /*
    * The function to post a specified employee to the http://localhost:4200/add URL
    **/
    $scope.postEmployee = function (firstname, lastname, phonenumber, mail){

        var data = {
            firstname: firstname,
            lastname: lastname,
            phonenumber: phonenumber,
            email: mail
        };
        $http.post("http://localhost:4200/add", JSON.stringify(data)).then(
            function successCallback(response){
                $scope.response = response;
                console.log(response);
            },
            function errorCallback(reponse) {
                $scope.response = response;
            }
        );
    };

    /*
    * The function to search for an employee by the ID on the http://localhost:4200/ID URL
    **/
    $scope.getEmployeesByID = function (){
        $scope.employeesByID = [];
        $http.get("http://localhost:4200/" + $scope.id).then(
            function successCallback(employeesByID) {
                $scope.employeesByID = employeesByID;
                console.log(employeesByID);
            },
            function errorCallback(employeesByID) {
            }
        );
    };

    /*
    * The function to delete an employee specified by the ID in the URL on http://localhost:4200/delete/ID
    **/
    $scope.deleteEmployeesWithID = function (){
        $http.delete("http://localhost:4200/delete/" + $scope.id).then(
            function successCallback(response){
                $scope.response = response;
                console.log("success");
            },
            function errorCallback(response){
                $scope.response = response;
            }
        );
    };

});
