package test

import (
	"EmployeeRegisterBackend/EmployeeRegisterBackend/main/api"
	"database/sql"
	"encoding/json"
	_ "fmt"
	"github.com/gorilla/mux"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"strings"
	"testing"
)

var employeeId int

func RouterTest() *mux.Router {
	router := api.Router()
	return router
}

//Fake Service
type DataBaseServiceMock struct {
	responseGetAll func() []api.Employee
	responseGet    func() api.Employee
	responsePost   func()
	responseDelete func() error
}

func (d DataBaseServiceMock) GetAllService() []api.Employee {
	return d.responseGetAll()
}

func (d DataBaseServiceMock) CreateEmployeeService(employee api.Employee) error {
	return nil
}

func (d DataBaseServiceMock) GetEmployeeByIdService(id string) api.Employee {
	if id == "1" {
		return d.responseGet()
	} else {
		var empty api.Employee
		return empty
	}
}

func (d DataBaseServiceMock) DeleteEmployeeService(id string) error {
	return nil
}

func (d DataBaseServiceMock) OpenDatabase(sqlDb string) *sql.DB {
	panic("implement me")
}

func (d DataBaseServiceMock) CloseDatabase(db *sql.DB) {
	panic("implement me")
}

//Testdaten

var employeeOne = api.Employee{ID: "1", FirstName: "Max", LastName: "Mustermann", PhoneNumber: "123", Email: "Max@example.com"}
var employeeTwo = api.Employee{ID: "2", FirstName: "Hans", LastName: "Mustermann", PhoneNumber: "123", Email: "Hans@example.com"}
var BodyString = "{\"firstName\":\"Max\",\"lastName\":\"Mustermann\",\"phoneNumber\":\"123\",\"email\":\"max@example.com\"}"

var employees []api.Employee
var employeesTwo []api.Employee

func generateMockArray() {
	employees = append(employees, employeeOne)
	employees = append(employees, employeeTwo)
	employeesTwo = append(employeesTwo, employeeOne)
}

//New Tests

func TestGetAll(t *testing.T) {
	serviceMock := DataBaseServiceMock{}
	serviceSave := api.DatabaseService
	serviceMock.responseGetAll = func() []api.Employee {
		return employees
	}
	api.DatabaseService = serviceMock

	request, _ := http.NewRequest("GET", "/", nil)
	response := httptest.NewRecorder()
	RouterTest().ServeHTTP(response, request)
	var employeeData []api.Employee
	err := json.NewDecoder(response.Body).Decode(&employeeData)
	if err != nil {
		t.Errorf("Data should be in JSON format")
	}
	if !reflect.DeepEqual(employeeData, employees) {
		t.Errorf("Data should match")
	}
	api.DatabaseService = serviceSave
}

func TestCreateEmployeeValid(t *testing.T) {
	serviceMock := DataBaseServiceMock{}
	serviceSave := api.DatabaseService
	api.DatabaseService = serviceMock

	request, _ := http.NewRequest("POST", "/add", strings.NewReader(BodyString))
	response := httptest.NewRecorder()
	RouterTest().ServeHTTP(response, request)
	if response.Code != 200 {
		t.Errorf("response Code should be 200")
	}
	api.DatabaseService = serviceSave
}

func TestCreateEmployeeInvalid(t *testing.T) {
	serviceMock := DataBaseServiceMock{}
	serviceSave := api.DatabaseService
	api.DatabaseService = serviceMock

	request, _ := http.NewRequest("POST", "/add", strings.NewReader(""))
	response := httptest.NewRecorder()
	RouterTest().ServeHTTP(response, request)
	if response.Code != 400 {
		t.Errorf("response Code should be 400")
	}
	api.DatabaseService = serviceSave
}

func TestGetEmployeeByIdValid(t *testing.T) {
	serviceMock := DataBaseServiceMock{}
	serviceSave := api.DatabaseService
	serviceMock.responseGet = func() api.Employee {
		return employeeOne
	}
	api.DatabaseService = serviceMock

	var path string = "/1"
	request, _ := http.NewRequest("GET", path, nil)
	response := httptest.NewRecorder()
	RouterTest().ServeHTTP(response, request)

	var employee api.Employee
	err := json.NewDecoder(response.Body).Decode(&employee)

	if err != nil {
		t.Errorf("Data should be in JSON format")
	}
	if employeeOne != employee {
		t.Errorf("Data should match")
	}

	if response.Code != 200 {
		t.Errorf("response Code should be 200")
	}
	api.DatabaseService = serviceSave
}

func TestGetEmployeeByIdInvalid(t *testing.T) {
	serviceMock := DataBaseServiceMock{}
	serviceSave := api.DatabaseService
	serviceMock.responseGet = func() api.Employee {
		return employeeOne
	}
	api.DatabaseService = serviceMock

	var path string = "/3"
	request, _ := http.NewRequest("GET", path, nil)
	response := httptest.NewRecorder()
	RouterTest().ServeHTTP(response, request)

	var empty api.Employee
	var employee api.Employee
	_ = json.NewDecoder(response.Body).Decode(&employee)

	if employee != empty {
		t.Errorf("Data should be empty")
	}
	if response.Code != 404 {
		t.Errorf("response Code should be 404")
	}
	api.DatabaseService = serviceSave
}

func TestDeleteEmployeeValid(t *testing.T) {
	serviceMock := DataBaseServiceMock{}
	serviceSave := api.DatabaseService
	serviceMock.responseGet = func() api.Employee {
		return employeeOne
	}
	api.DatabaseService = serviceMock

	var path string = "/delete/1"
	request, _ := http.NewRequest("DELETE", path, nil)
	response := httptest.NewRecorder()
	RouterTest().ServeHTTP(response, request)
	if response.Code != 200 {
		t.Errorf("response Code should be 200")
	}
	api.DatabaseService = serviceSave
}

func TestDeleteEmployeeInvalid(t *testing.T) {
	serviceMock := DataBaseServiceMock{}
	serviceSave := api.DatabaseService
	serviceMock.responseGet = func() api.Employee {
		return employeeOne
	}
	api.DatabaseService = serviceMock

	var path string = "/delete/3"
	request, _ := http.NewRequest("DELETE", path, nil)
	response := httptest.NewRecorder()
	RouterTest().ServeHTTP(response, request)
	if response.Code != 404 {
		t.Errorf("response Code should be 404")
	}
	api.DatabaseService = serviceSave
}

//Test Main

func TestMain(m *testing.M) {

	generateMockArray()
	code := m.Run()
	os.Exit(code)
}

//var save api.DatabaseService
