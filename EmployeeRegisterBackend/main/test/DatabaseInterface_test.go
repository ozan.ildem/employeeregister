package test

import (
	"EmployeeRegisterBackend/EmployeeRegisterBackend/main/api"
	"testing"
)

func TestOpenDatabaseValid(t *testing.T) {
	db := api.DatabaseInterface.OpenDatabase("employeeService:123@tcp(localhost:3306)/employeemanager")
	if db == nil {
		t.Errorf("Database should be open")
	}
}

func TestOpenDatabaseInvalid(t *testing.T) {
	db := api.DatabaseInterface.OpenDatabase("")
	if db != nil {
		t.Errorf("Database should be nil")
	}
}

func TestQuery(t *testing.T) {
	_, err := api.DatabaseInterface.Query("SELECT * FROM employeedata")
	if err != nil {
		t.Errorf("Should be run without Errors")
	}
}
