package test

import (
	"EmployeeRegisterBackend/EmployeeRegisterBackend/main/api"
	"database/sql"
	"reflect"
	"testing"
)

var sqlStringTest bool

type DatabaseInterfaceMock struct {
	response func(string) ([]api.Employee, error)
}

func (d DatabaseInterfaceMock) OpenDatabase(sqlDb string) *sql.DB {
	return nil
}

func (d DatabaseInterfaceMock) CloseDatabase(db *sql.DB) {
	return
}

func (d DatabaseInterfaceMock) Query(stmt string) ([]api.Employee, error) {
	return d.response(stmt)
}

func TestGetAllServiceValid(t *testing.T) {
	databaseMock := DatabaseInterfaceMock{}
	databaseMock.response = func(s string) ([]api.Employee, error) {
		return employees, nil
	}
	api.DatabaseInterface = databaseMock
	result := api.DatabaseService.GetAllService()
	if !reflect.DeepEqual(result, employees) {
		t.Errorf("Data should match")
	}
}

func TestGetAllServiceInvalid(t *testing.T) {
	databaseMock := DatabaseInterfaceMock{}
	databaseMock.response = func(s string) ([]api.Employee, error) {
		var emptyArray []api.Employee
		return emptyArray, nil
	}
	api.DatabaseInterface = databaseMock
	result := api.DatabaseService.GetAllService()
	if result != nil {
		t.Errorf("Data should be empty")
	}
}

func TestGetByIdServiceValid(t *testing.T) {
	databaseMock := DatabaseInterfaceMock{}
	databaseMock.response = func(s string) ([]api.Employee, error) {
		return employeesTwo, nil
	}
	api.DatabaseInterface = databaseMock
	result := api.DatabaseService.GetEmployeeByIdService("1")
	if !reflect.DeepEqual(result, employeeOne) {
		t.Errorf("Data should match")
	}
}

func TestGetByIdServiceInvalid(t *testing.T) {
	databaseMock := DatabaseInterfaceMock{}
	databaseMock.response = func(s string) ([]api.Employee, error) {
		//var emptyArray []api.Employee
		return nil, nil
	}
	api.DatabaseInterface = databaseMock
	result := api.DatabaseService.GetEmployeeByIdService("3")
	var empty api.Employee
	if result != empty {
		t.Errorf("Data should be empty")
	}
}
func TestDeleteEmployeeServiceValid(t *testing.T) {
	sqlStringTest = false
	databaseMock := DatabaseInterfaceMock{}
	databaseMock.response = func(s string) ([]api.Employee, error) {
		if s == "DELETE FROM employeedata WHERE id = '1'" {
			sqlStringTest = true
		}
		return nil, nil
	}
	api.DatabaseInterface = databaseMock
	api.DatabaseService.DeleteEmployeeService("1")
	if sqlStringTest == false {
		t.Errorf("wrong sql statement")
	}
	sqlStringTest = false
}

func TestDeleteEmployeeServiceInvalid(t *testing.T) {
	sqlStringTest = false
	databaseMock := DatabaseInterfaceMock{}
	databaseMock.response = func(s string) ([]api.Employee, error) {
		if s == "DELETE FROM employeedata WHERE id = '1'" {
			sqlStringTest = true
		}
		return nil, nil
	}
	api.DatabaseInterface = databaseMock
	api.DatabaseService.DeleteEmployeeService("3")
	if sqlStringTest == true {
		t.Errorf("should be wrong sql statement")
	}
	sqlStringTest = false
}

func TestCreateEmployeeServiceValid(t *testing.T) {
	sqlStringTest = false
	databaseMock := DatabaseInterfaceMock{}
	databaseMock.response = func(s string) ([]api.Employee, error) {
		if s == "INSERT INTO employeedata(firstName,lastName,phoneNumber,email) VALUES('Max', 'Mustermann', '123', 'Max@example.com')" {
			sqlStringTest = true
		}
		return nil, nil
	}
	api.DatabaseInterface = databaseMock
	api.DatabaseService.CreateEmployeeService(employeeOne)
	if sqlStringTest == false {
		t.Errorf("wrong sql statement")
	}
	sqlStringTest = false
}

func TestCreateEmployeeServiceInvalid(t *testing.T) {
	sqlStringTest = false
	databaseMock := DatabaseInterfaceMock{}
	databaseMock.response = func(s string) ([]api.Employee, error) {
		if s == "INSERT INTO employeedata(firstName,lastName,phoneNumber,email) VALUES('Max', 'Mustermann', '123', 'Max@example.com')" {
			sqlStringTest = true
		}
		return nil, nil
	}
	api.DatabaseInterface = databaseMock
	var empty api.Employee
	api.DatabaseService.CreateEmployeeService(empty)

	if sqlStringTest == true {
		t.Errorf("should be wrong sql statement")
	}
	sqlStringTest = false

}
