package api

import "database/sql"

type Employee struct {
	ID          string `json:"id"`
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`
	PhoneNumber string `json:"phoneNumber"`
	Email       string `json:"email"`
}

type Service interface {
	GetAllService() []Employee
	CreateEmployeeService(employee Employee) error
	GetEmployeeByIdService(id string) Employee
	DeleteEmployeeService(id string) error
}

type Database interface {
	OpenDatabase(sqlDb string) *sql.DB
	Query(stmt string) ([]Employee, error)
}
