package api

import "database/sql"

const sqlDb string = "employeeService:123@tcp(localhost:3306)/employeemanager"

type DatabaseImpl struct{}

var (
	DatabaseInterface Database = DatabaseImpl{}
)

func (d DatabaseImpl) OpenDatabase(sqlDb string) *sql.DB {
	db, err := sql.Open("mysql", sqlDb)
	err = db.Ping()

	if err != nil {
		return nil
	}
	return db
}

func (d DatabaseImpl) Query(stmt string) ([]Employee, error) {
	db := d.OpenDatabase(sqlDb)
	statement, _ := db.Prepare(stmt)
	rows, err := statement.Query()
	db.Close()
	var out []Employee
	for rows.Next() {
		var employee Employee
		_ = rows.Scan(&employee.ID, &employee.FirstName, &employee.LastName, &employee.PhoneNumber, &employee.Email)
		out = append(out, employee)
	}
	return out, err
}
