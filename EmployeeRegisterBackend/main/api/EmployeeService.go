package api

import (
	_ "fmt"
	_ "github.com/go-sql-driver/mysql"
)

type ServiceImpl struct{}

var (
	DatabaseService Service = ServiceImpl{}
)

func (s ServiceImpl) GetAllService() []Employee {
	out, _ := DatabaseInterface.Query("SELECT * FROM employeedata")
	if len(out) > 0 {
		return out
	}
	var empty []Employee
	return empty
}

func (s ServiceImpl) GetEmployeeByIdService(id string) Employee {

	var stmt string = "SELECT * FROM employeedata WHERE id = '" + id + "'"
	result, _ := DatabaseInterface.Query(stmt)
	if len(result) > 0 {
		return result[0]
	}
	var empty Employee
	return empty
}

func (s ServiceImpl) DeleteEmployeeService(id string) error {

	stmt := "DELETE FROM employeedata WHERE id = '" + id + "'"
	_, err := DatabaseInterface.Query(stmt)
	return err
}

func (s ServiceImpl) CreateEmployeeService(employee Employee) error {

	var stmt string = "INSERT INTO employeedata(firstName,lastName,phoneNumber,email) VALUES('" +
		employee.FirstName + "', '" +
		employee.LastName + "', '" +
		employee.PhoneNumber + "', '" +
		employee.Email + "')"
	_, err := DatabaseInterface.Query(stmt)
	return err
}
