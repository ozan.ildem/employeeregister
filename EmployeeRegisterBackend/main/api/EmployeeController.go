package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

const origin string = "Access-Control-Allow-Origin"
const contentType string = "Content-Type"
const appJSON string = "application/json"

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set(origin, "*")
}

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set(origin, "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func Router() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/", GetAll).Methods("GET")
	router.HandleFunc("/add", CreateEmployee).Methods("POST", "OPTIONS", "PUT", "DELETE")
	router.HandleFunc("/delete/{id}", DeleteEmployee).Methods("DELETE", "OPTIONS")
	router.HandleFunc("/{id}", GetEmployeeById).Methods("GET")
	return router
}

func GetEmployeeById(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	w.Header().Set(contentType, appJSON)
	params := mux.Vars(r)
	out := DatabaseService.GetEmployeeByIdService(params["id"])

	if out.ID == "" {
		http.Error(w, "ID not found", 404)
		return
	}

	json.NewEncoder(w).Encode(out)
}

func GetAll(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	w.Header().Set(contentType, appJSON)
	out := DatabaseService.GetAllService()
	json.NewEncoder(w).Encode(out)
}

func CreateEmployee(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	setupResponse(&w, r)
	w.Header().Set(contentType, appJSON)

	if r.Method == "POST" {

		var employee Employee
		_ = json.NewDecoder(r.Body).Decode(&employee)

		var empty Employee
		if employee == empty {
			http.Error(w, "Invalid employee", 400)
			return
		}
		DatabaseService.CreateEmployeeService(employee)
	}
}

func DeleteEmployee(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	enableCors(&w)
	setupResponse(&w, r)
	w.Header().Set(contentType, appJSON)

	employee := DatabaseService.GetEmployeeByIdService(params["id"])

	if employee.ID == "" {
		http.Error(w, "ID not found", 404)
		return
	}

	DatabaseService.DeleteEmployeeService(params["id"])
}
