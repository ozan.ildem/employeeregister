package main

import (
	"EmployeeRegisterBackend/EmployeeRegisterBackend/main/api"
	"log"
	"net/http"
)

func main() {
	router := api.Router()
	log.Fatal(http.ListenAndServe(":4200", router))
}
