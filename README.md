------QUICK START-------


To start the Frontend go into the EmployeeRegisterFrontend directory and navigate to the same directory.
with the Terminal use "npm start" to start the Frontend on Localhost:8000
if you do not have AngularJS dependencies installed run "npm install" first.

Further information is available in the README.md inside of the EmployeeRegisterFrontend directory.

